# Commands for Remote to Local Repository.

In order to update changes from local to remote when we cloned the remote repository on local machine, first we need to create app password for the remote repository to perform a push and some clone operations.
Once the app password is created and saved then run thew following commands,

- "mkdir repos"
- "cd repos"
- "git clone https://deepalikandekar4-admin@bitbucket.org/deepalikandekar4/demorepo2.git"
- "ls -a"
- "cd cloned_repo_name"
- "ls -a"

** Make some changes to README.md or other files **


- "git add README.md"
- git commit -m "Local to remote Test"

** Sync remote and local **


- git push -u origin master

After running this push command it will ask for authorize provide password that which we have created then apporve the it will automatically update the local changes in remote repository.

Then check the changes in the remote repo. 
