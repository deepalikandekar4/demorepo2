# Demo on 'git' repository. 

## Introduction 
This ** git repository ** is created as a demo remote repo for the Quiz.

- Git is a distributed version control system that we can install on our local machines that enables us to collaborate others in activities such as softwarer development.
- Bitbucket and github are DVCS application platforms whrere 'git' is used.

# Basic Commands to Create a Repository
- 'git init'   : create a local repo
- 'git config' : store config info such as name, email etc
- 'git add'    : to track the file
- 'git status' : to check status of file
- 'git commit' : create new version
- 'git log'    : version info
- 'git push'   : push changes from local to remote -sync

# Advanced Commands 

- 'git pull'   : pull changes from local to remote and update -sync
- 'git fetch'  : push changes from local to remote but do not update local
- 'git merge'  : update local repo using the result of a fetch
- 'git checkout' : go to a branch to view or make changes
- 'git branch' :create/delete branch
- 'git tag'    : add tag to a commit

END
